// Menu show 
var navMenu = document.getElementById('nav-menu'),
    openMenu = document.getElementById('open-menu'),
    closeMenu = document.getElementById('nav-close')

// Show
openMenu.addEventListener('click', ()=> {
    navMenu.classList.toggle('show')
})

// Hidden
closeMenu.addEventListener('click', ()=> {
    navMenu.classList.remove('show')
})

//  Remove Menu  
const navLink = document.querySelectorAll('.nav-link')

function linkAction(){
    navMenu.classList.remove('show')
}
navLink.forEach(n => n.addEventListener('click', linkAction))


// -----------------------------------------------------

// SCROLL SHADOW'S HEADER
// mudar o header ao rolar a página
const header = document.querySelector('header')
const navHeight = header.offsetHeight
function changeHeaderWhenScroll() {
    if(window.scrollY >= navHeight) {
        // maior que a altura do header
        header.classList.add('scroll')

    } else {
        // menor que a altura do header
        header.classList.remove('scroll')

    }
}

// -----------------------------------------------------

// SCROLLREVEAL
// Mostrar elementos quando rolar a página
const scrollReveal = ScrollReveal({
    origin: 'top',
    distance: '50px',
    duration: 750,
    reset: true
})

scrollReveal.reveal(
    `#home .home-text, #home .home-img,
     #card,
     #about .content-about-text, #about .about-img,
     #contact

    `, { interval: 100 }
)

// -----------------------------------------------------

// BUTTON TOP SHOW
// pegar o botão da tela
const backToTopButton = document.querySelector('.back-to-top')
function backtoTop() {
    if(window.scrollY >= 560) {
        backToTopButton.classList.add('show')

    } else {
        backToTopButton.classList.remove('show')
    }
}

// -----------------------------------------------------

window.addEventListener('scroll', function () {
    changeHeaderWhenScroll()
    backtoTop()
})
